//////////////////////////////////////////////////////////////////////////
//
//	created:	2006/08/15
//	file name:	edif.inc
//
//////////////////////////////////////////////////////////////////////////
//
//	Copyright (c) 2006 Elecard Ltd.
//	All rights are reserved.  Reproduction in whole or in part is prohibited
//	without the written consent of the copyright owner.
//
//	Elecard Ltd. reserves the right to make changes without
//	notice at any time. Elecard Ltd. makes no warranty, expressed,
//	implied or statutory, including but not limited to any implied
//	warranty of merchantability of fitness for any particular purpose,
//	or that the use will not infringe any third party patent, copyright
//	or trademark.
//
//	Elecard Ltd. must not be liable for any loss or damage arising
//	from its use.
//
//////////////////////////////////////////////////////////////////////////
//
//	author:  Sergey Nikiforov, nick@elecard.net.ru
//	
//	purpose: Filter's & media types's GUIDs
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#ifndef RC_INVOKED // don't let resource compiler see this part

//Parameters GUIDs are available in FULL version of SDK

#endif