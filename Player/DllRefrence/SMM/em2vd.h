//////////////////////////////////////////////////////////////////////////
//
//	created:	2005/04/12
//	file name:	EM2VD.h
//
//////////////////////////////////////////////////////////////////////////
//
//	Copyright (c) 2005-2006 Elecard Ltd.
//	All rights are reserved.  Reproduction in whole or in part is prohibited
//	without the written consent of the copyright owner.
//
//	Elecard Ltd. reserves the right to make changes without
//	notice at any time. Elecard Ltd. makes no warranty, expressed,
//	implied or statutory, including but not limited to any implied
//	warranty of merchantability of fitness for any particular purpose,
//	or that the use will not infringe any third party patent, copyright
//	or trademark.
//
//	Elecard Ltd. must not be liable for any loss or damage arising
//	from its use.
//
//////////////////////////////////////////////////////////////////////////
//
//	author:  Sergey Silkin (e-mail: ssg@elecard.net.ru)
//	
//	purpose: The definition of the Elecarg MPEG-2 Video Decoder filter's CLSID and CLSIDs of its parameters
//

#ifndef __EM2VD_FILTER_PROPID__
#define __EM2VD_FILTER_PROPID__

//Parameters GUIDs are available in FULL version of SDK

#endif //__EM2VD_FILTER_PROPID__

