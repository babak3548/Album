Before using AVITrimmerCOM object in VB.Net project perform next steps: 
1. Click menu "Project->Add Reference"
2. Choose COM tab in dialog
3. Choose "AVITrimmerCOM 1.0 Type Library" in list of components and click "Select" button
4. Click "OK" button. 