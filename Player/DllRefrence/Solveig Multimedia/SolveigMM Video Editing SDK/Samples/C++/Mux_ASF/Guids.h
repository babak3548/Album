#pragma once

static const GUID MEDIASUBTYPE_H264_ANNEXB = 
{0x8D2D71CB, 0x243F, 0x45E3, {0xB2, 0xD8, 0x5F, 0xD7, 0x96, 0x7E, 0xC0, 0x9B}};

static const GUID MEDIASUBTYPE_H264_2 = 
{0x31435641, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_H264_3 = 
{0x48535356, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_H264_4 = 
{0x34363248, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_MPEG4 = 
{0x7634706D, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_AC3 = 
{0x00002000, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_AAC = 
{0x000000FF, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_MP3 = 
{0x00000055, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_VOXWARE_AUDIO = 
{0x00000075, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_DIVX = 
{0x30355844, 0x0000 ,0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_DIVX_2 = 
{0x33564944, 0x0000 ,0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_DIVX_3 = 
{0x58564944, 0x0000 ,0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_XVID = 
{0x44495658, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_OGG =
{0xd2855fa9, 0x61a7, 0x4db0, {0xb9, 0x79, 0x71, 0xf2, 0x97, 0xc1, 0x7a, 0x4}};

static const GUID MEDIASUBTYPE_OGG_1 =
{0x0000676F, 0x0000 ,0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID MEDIASUBTYPE_VORBIS = 
{0xcddca2d5, 0x6d75, 0x4f98, {0x84, 0x0e, 0x73, 0x7b, 0xed, 0xd5, 0xc6, 0x3b}};


//Windows Media Guids

static const GUID WMMEDIASUBTYPE_WMV1 =
{0x31564D57, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID WMMEDIASUBTYPE_WMV2 =
{0x32564D57, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID WMMEDIASUBTYPE_WMV3 =
{0x33564D57, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID WMMEDIASUBTYPE_WMAudioV7 =
{0x00000161, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID WMMEDIASUBTYPE_WMAudioV8 =
{0x00000161, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID WMMEDIASUBTYPE_WMAudioV9 =
{0x00000162, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};

static const GUID WMMEDIASUBTYPE_WMSP1 =
{0x0000000A, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71}};
