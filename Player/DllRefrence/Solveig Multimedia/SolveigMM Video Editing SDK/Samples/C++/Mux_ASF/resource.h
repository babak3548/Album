//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TestMuxerApp.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTMUXERAPP_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_ENCODER_DIALOG              130
#define IDB_BITMAP1                     132
#define IDB_LOGO_BITMAP                 134
#define IDB_BITMAP2                     138
#define IDC_INPUTFILE_EDIT              1000
#define IDC_OUTPUTFILE_EDIT             1001
#define IDC_INPUTFILE_BUTTON            1002
#define IDC_OUTPUTFILE_BUTTON           1003
#define IDC_START_BUTTON                1004
#define IDC_CANCEL_BUTTON               1005
#define IDC_BUILD_GRAPH_BUTTON          1006
#define IDC_TREAT_VIDEO_AS_H264_CHECK   1007
#define IDC_INDEX_ASF_CHECK             1008
#define IDC_WRITE_LOG_CHECK             1009
#define IDC_SHOW_PROPS_BUTTON           1012
#define IDC_TRANSCODE_VIDEO_CHECK       1013
#define IDC_USE_CUSTOM_FOURCC_H264_CHECK 1014
#define IDC_USE_CUSTOM_FOURCC_CHECK     1014
#define IDC_H264_FOURCC_EDIT            1015
#define IDC_FOURCC_EDIT                 1015
#define IDC_AUDIO_TRANSCODE_CHECK       1016
#define IDC_VIDEO_ENCODER_EDIT          1017
#define IDC_VIDEO_ENCODER_BUTTON        1018
#define IDC_AUDIO_STREAM_COMBO          1020
#define IDC_AUDIO_ENCODER_EDIT          1021
#define IDC_VIDEO_ENCODER_BUTTON2       1022
#define IDC_AUDIO_ENCODER_BUTTON        1022
#define IDC_VIDEOENABLE_CHECK           1023
#define IDC_AUDIOENABLE_CHECK           1024
#define IDC_AUDIO_UNCOKPRESSED_CHECK    1025
#define IDC_VIDEO_UNCOKPRESSED_CHECK    1026
#define IDC_PROGRESS                    1027
#define IDC_PROGRESS_STATIC             1028
#define IDC_AUDIO_LANG_ID               1029
#define IDC_LIST1                       1031
#define IDC_LIST2                       1032
#define IDC_VIDEO_GROUP                 1034
#define IDC_AUDIO_GROUP                 1035
#define IDC_COMBO1                      1036
#define IDC_CHECK1                      1037
#define IDC_USE_LANG                    1037

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1038
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
