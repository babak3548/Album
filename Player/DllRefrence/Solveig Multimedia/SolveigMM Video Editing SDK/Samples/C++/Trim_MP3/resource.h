//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Trimmer.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TRIMMER_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDR_TOOLBAR1                    129
#define IDD_OUTPUT_DIALOG               131
#define IDD_SETTINGS_DIALOG             132
#define IDB_BITMAP1                     136
#define IDC_SLIDER                      1000
#define IDC_START_TIME                  1001
#define IDC_STOP_TIME                   1002
#define IDC_EDIT_STEP                   1003
#define IDC_MAXLENGTH                   1006
#define IDC_CURRENTPOS                  1007
#define IDC_VOLUME_SLIDER               1008
#define IDC_EDIT1                       1009
#define IDC_COMPUTER_ID                 1010
#define IDC_BUTTON_SELECTFOLDER         1011
#define IDC_OUTPUT_FOLDER               1012
#define IDC_PROGRESS1                   1013
#define IDC_LINKTOSITE                  1015
#define IDC_RADIO_FASTTRIM              1018
#define IDC_RADIO_NORMALTRIM            1019
#define IDC_STATIC_VIDEOWINDOW          1020
#define IDC_PROGRESS                    1021
#define IDC_BUTTON_CANCEL               1022
#define IDC_CHECK_LOOK_AT_SAMPLES_TIMES 1023
#define ID_BUTTON_OPEN                  32771
#define ID_BUTTON_PLAY                  32774
#define ID_BUTTON_PAUSE                 32775
#define ID_BUTTON_STOP                  32777
#define ID_BUTTON_BACK                  32780
#define ID_BUTTON_NEXT                  32781
#define ID_BUTTON_DELETE                32783
#define ID_BUTTON_LEFTFLAG              32784
#define ID_BUTTON_RIGHTFLAG             32789
#define ID_BUTTON_TRIM                  32791
#define ID_BUTTON_SETTINGS              32792
#define IDS_STRING_INDICATORS_STATUS    47548
#define IDS_STRING_INDICATORS_FILENAME  47548

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32798
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
