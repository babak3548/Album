//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SimplePlayer.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SIMPLEPLAYER_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDI_ICON_BEGIN                  129
#define IDI_ICON_BACKWARD               130
#define IDI_ICON_STEPBW                 131
#define IDI_ICON_PLAY                   132
#define IDI_ICON_PAUSE                  133
#define IDI_ICON_STOP                   134
#define IDI_ICON_STEPFW                 135
#define IDI_ICON_FORWARD                136
#define IDI_ICON_END                    137
#define IDI_ICON_OPEN                   138
#define IDI_ICON_FULLSCREEN             139
#define IDR_ACCELERATOR_APP             140
#define IDI_ICON_INDEX                  140
#define IDI_ICON_MARK                   141
#define IDI_ICON_SAVE                   142
#define IDI_ICON_CLOSE                  143
#define IDD_DIALOG_INDEX                144
#define IDI_ICON_SELINTERVAL            147
#define IDI_ICON_DESELINTERVAL          148
#define IDI_ICON_ADD                    149
#define IDI_ICON_REMOVE                 150
#define IDC_SLD_POSITION                1000
#define IDC_BACKWARD                    1001
#define IDC_TIME                        1002
#define IDC_PLAY                        1003
#define IDC_STOP                        1004
#define IDC_FORWARD                     1005
#define IDC_TIME_BEGIN                  1005
#define IDC_ED_FILE                     1006
#define IDC_OPEN                        1007
#define IDC_BEGIN                       1008
#define IDC_STEPBW                      1009
#define IDC_STEPFW                      1010
#define IDC_LIST                        1011
#define IDC_FULLSCREEN                  1012
#define IDC_END                         1013
#define IDC_DURATION                    1014
#define IDC_PAUSE                       1015
#define IDC_ED_FILE_SAVE                1016
#define IDC_SAVE                        1017
#define IDC_ADD                         1018
#define IDC_REMOVE                      1019
#define IDC_MARK_RIGHT                  1020
#define IDC_DURATION2                   1020
#define IDC_TIME_END                    1020
#define IDC_MARK_LEFT                   1021
#define IDC_PROGRESS                    1022
#define IDC_TRIM                        1023
#define IDC_PROGRESS_INDEX              1024
#define IDC_CHECK_INDEX                 1026
#define IDC_CHECK2                      1027
#define IDC_CHECK_VIDEO_ONLY            1027
#define IDC_SMMSLIDERCTRL1              1028
#define IDC_SMMSLIDERCTRL               1028
#define IDC_SELECT_INTERVAL             1029
#define IDC_DESELECT_INTERVAL           1030
#define ID_ACCEL_FullScreen             32771
#define ID_ACCEL_Play                   32772
#define ID_ACCEL_Begin                  32773
#define ID_ACCEL_End                    32774
#define ID_ACCEL_StepBW                 32775
#define ID_ACCEL_StepFW                 32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        151
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1031
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
