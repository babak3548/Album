//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MKVMuxerApp.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MKVMUXERAPP_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDI_LOGO                        129
#define IDB_BITMAP_LOGO                 130
#define IDC_INPUTFILE_EDIT              1000
#define IDC_OUTPUTFILE_EDIT             1001
#define IDC_INPUTFILE_BUTTON            1002
#define IDC_OUTPUTFILE_BUTTON           1003
#define IDC_START_BUTTON                1004
#define IDC_SUBTITLES_GROUP             1006
#define IDC_SUBTITLE_STREAM_COMBO       1007
#define IDC_SUBTITLEENABLE_CHECK        1008
#define IDC_VIDEO_STREAM_COMBO          1009
#define IDC_BUTTON_ADVANCED             1010
#define IDC_EDIT_AUDIO_TRACK_NAME       1011
#define IDC_EDIT_VIDEO_TRACK_NAME       1012
#define IDC_EDIT_SUBTITLE_TRACK_NAME    1013
#define IDC_COMBO_AUDIO_LANG            1014
#define IDC_COMBO_SUBTITLE_LANG         1015
#define IDC_COMBO_VIDEO_LANG            1016
#define IDC_CHECK_AUDIO_FLAGDEFAULT     1017
#define IDC_CHECK_AUDIO_FLAGFORCED      1018
#define IDC_CHECK_SUBTITLE_FLAGDEFAULT  1019
#define IDC_AUDIO_STREAM_COMBO          1020
#define IDC_CHECK_AUDIO_FLAGFORCED2     1021
#define IDC_CHECK_SUBTITLE_FLAGFORCED   1021
#define IDC_CHECK_VIDEO_FLAGDEFAULT     1022
#define IDC_VIDEOENABLE_CHECK           1023
#define IDC_AUDIOENABLE_CHECK           1024
#define IDC_CHECK_VIDEO_FLAGFORCED      1025
#define IDC_PROGRESS                    1027
#define IDC_COMBO_OUT_TYPE              1028
#define IDC_VIDEO_GROUP                 1034
#define IDC_AUDIO_GROUP                 1035

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1029
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
