; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAVITrimmerAppDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "AviTrimmerApp.h"

ClassCount=3
Class1=CAviTrimmerAppApp
Class2=CAVITrimmerAppDlg
Class3=CAboutDlg

ResourceCount=5
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_AVITRIMMERAPP_DIALOG
Resource4=IDD_ABOUTBOX (English (U.S.))
Resource5=IDD_AVITRIMMERAPP_DIALOG (English (U.S.))

[CLS:CAVITrimmerAppApp]
Type=0
HeaderFile=AviTrimmerApp.h
ImplementationFile=AviTrimmerApp.cpp
Filter=N

[CLS:CAVITrimmerAppDlg]
Type=0
HeaderFile=AviTrimmerAppDlg.h
ImplementationFile=AviTrimmerAppDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_CHECK1

[CLS:CAboutDlg]
Type=0
HeaderFile=AviTrimmerAppDlg.h
ImplementationFile=AviTrimmerAppDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Class=CAboutDlg


[DLG:IDD_AVITRIMMERAPP_DIALOG]
Type=1
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Class=CAVITrimmerAppDlg

[DLG:IDD_AVITRIMMERAPP_DIALOG (English (U.S.))]
Type=1
Class=CAVITrimmerAppDlg
ControlCount=20
Control1=IDC_EDITFILEIN,edit,1350631552
Control2=IDC_DIALOGIN,button,1342242816
Control3=IDC_EDITFILEOUT,edit,1350631552
Control4=IDC_DIALOGOUT,button,1342242816
Control5=IDC_STATIC,button,1342177287
Control6=IDC_STARTIME,edit,1350639744
Control7=IDC_STOPTIME,edit,1350639744
Control8=IDC_STATIC,button,1342177287
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_RUN,button,1342177281
Control13=IDC_STOP,button,1342242816
Control14=IDCANCEL,button,1342242816
Control15=IDC_STATIC,button,1342177287
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_PROGRESS,msctls_progress32,1350565889
Control19=IDC_STATIC,static,1342308352
Control20=IDC_CHECK1,button,1208025091

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=CAboutDlg
ControlCount=6
Control1=IDC_STATIC,static,1342308481
Control2=IDC_STATIC,static,1342308353
Control3=IDOK,button,1342373889
Control4=IDC_STATIC,static,1342308353
Control5=IDC_STATIC,static,1342177294
Control6=IDC_STATIC,static,1342308353

