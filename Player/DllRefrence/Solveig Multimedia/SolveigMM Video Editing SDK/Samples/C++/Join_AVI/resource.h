//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SimpleJoiner.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SIMPLEJOINER_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_PROGRESS             130
#define IDB_BITMAP1                     133
#define IDB_BITMAP2                     139
#define IDC_LIST1                       1000
#define IDC_FILE_LIST                   1000
#define IDC_PROGRESS                    1001
#define ID_FILE_OPENFILE                32771
#define ID_FILE_SAVEJOINLIST            32772
#define ID_FILE_STARTJOINING            32773
#define ID_LIST_MOVEUP                  32774
#define ID_LIST_MOVEDOWN                32775
#define ID_LIST_DELETESELECTEDITEMS     32776
#define ID_LIST_CLEARLIST               32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
