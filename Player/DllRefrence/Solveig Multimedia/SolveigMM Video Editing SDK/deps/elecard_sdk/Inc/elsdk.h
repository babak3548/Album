
/////////////////////////////////////////////////////////////////////////////////
//
//	   Copyright (c) 2005 Elecard Ltd.
//	   All rights are reserved.  Reproduction in whole or in part is prohibited
//	   without the written consent of the copyright owner.
//
//	   Elecard Ltd. reserves the right to make changes without
//	   notice at any time. Elecard Ltd. makes no warranty, expressed,
//	   implied or statutory, including but not limited to any implied
//	   warranty of merchantability of fitness for any particular purpose,
//	   or that the use will not infringe any third party patent, copyright
//	   or trademark.
//
//	   Elecard Ltd. must not be liable for any loss or damage arising
//	   from its use.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _SDK_INCLUDES_
#define _SDK_INCLUDES_

#pragma warning (disable : 4786)

#include <streams.h>
#include <atlbase.h>

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include <string>
#include <map>
#include <list>
#include <deque>
#include <vector>
#include <algorithm>

#include "..\Classes\ExMsg.h"
#include "..\Classes\eDllwrap.h"

#include "..\Classes\Utils.h"
#include "..\Classes\Elements.h"
#include "..\Classes\Managers.h"

#include "..\Classes\EncodeMng.h"

#include "..\Classes\SInfo.h"

//#include "..\Classes\GraphPreset.h"
//#include "..\Classes\XMLPresetParser.h"

#endif //_SDK_INCLUDES_