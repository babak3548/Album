
/////////////////////////////////////////////////////////////////////////////////
//
//	   Copyright (c) 2005 Elecard Ltd.
//	   All rights are reserved.  Reproduction in whole or in part is prohibited
//	   without the written consent of the copyright owner.
//
//	   Elecard Ltd. reserves the right to make changes without
//	   notice at any time. Elecard Ltd. makes no warranty, expressed,
//	   implied or statutory, including but not limited to any implied
//	   warranty of merchantability of fitness for any particular purpose,
//	   or that the use will not infringe any third party patent, copyright
//	   or trademark.
//
//	   Elecard Ltd. must not be liable for any loss or damage arising
//	   from its use.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _FILTER_CATEGORY_
#define _FILTER_CATEGORY_

// {F758276C-4B90-4e65-BD04-A8F198A1DE80}
static const GUID FCATEGORY_Unknown = 
{0xf758276c, 0x4b90, 0x4e65, {0xbd, 0x4, 0xa8, 0xf1, 0x98, 0xa1, 0xde, 0x80}};

// {73F40371-863C-4769-AA37-3C3838EDE9C5}
static const GUID FCATEGORY_Source =
{0x73f40371, 0x863c, 0x4769, {0xaa, 0x37, 0x3c, 0x38, 0x38, 0xed, 0xe9, 0xc5}};

// {F36BFC91-8173-4155-A92B-7D194B816EBF}
static const GUID FCATEGORY_Splitter = 
{0xf36bfc91, 0x8173, 0x4155, {0xa9, 0x2b, 0x7d, 0x19, 0x4b, 0x81, 0x6e, 0xbf}};

// {E26A91D5-602A-4d6e-91C3-50CA13586DE5}
static const GUID FCATEGORY_VideoDecoder = 
{0xe26a91d5, 0x602a, 0x4d6e, {0x91, 0xc3, 0x50, 0xca, 0x13, 0x58, 0x6d, 0xe5}};

// {189089E9-B1E7-4d07-8F03-D6FAE17B9251}
static const GUID FCATEGORY_AudioDecoder = 
{0x189089e9, 0xb1e7, 0x4d07, {0x8f, 0x3, 0xd6, 0xfa, 0xe1, 0x7b, 0x92, 0x51}};

// {5BAE48F9-F77F-4be0-83AD-392109EC42F1}
static const GUID FCATEGORY_VideoEncoder = 
{0x5bae48f9, 0xf77f, 0x4be0, {0x83, 0xad, 0x39, 0x21, 0x9, 0xec, 0x42, 0xf1}};

// {531D21ED-649E-4984-9C62-35C97E2E0A63}
static const GUID FCATEGORY_AudioEncoder = 
{0x531d21ed, 0x649e, 0x4984, {0x9c, 0x62, 0x35, 0xc9, 0x7e, 0x2e, 0xa, 0x63}};

// {D9F02271-5613-47f6-AC13-8ADAFD3254BA}
static const GUID FCATEGORY_VideoTransform = 
{0xd9f02271, 0x5613, 0x47f6, {0xac, 0x13, 0x8a, 0xda, 0xfd, 0x32, 0x54, 0xba}};

// {076EEC32-9E44-44a7-9FAE-42546331921D}
static const GUID FCATEGORY_VideoTransform2 = 
{0x76eec32, 0x9e44, 0x44a7, {0x9f, 0xae, 0x42, 0x54, 0x63, 0x31, 0x92, 0x1d}};

// {0E836779-6AF9-45a5-9CD0-276401F3167C}
static const GUID FCATEGORY_AudioTransform = 
{0xe836779, 0x6af9, 0x45a5, {0x9c, 0xd0, 0x27, 0x64, 0x1, 0xf3, 0x16, 0x7c}};

// {28E7902A-0F71-4ad3-BA65-246BB4EF2FA5}
static const GUID FCATEGORY_AudioTransform2 = 
{0x28e7902a, 0xf71, 0x4ad3, {0xba, 0x65, 0x24, 0x6b, 0xb4, 0xef, 0x2f, 0xa5}};

// {81AA8295-A2D8-412f-89CD-DB164878501F}
static const GUID FCATEGORY_Multiplexer =
{0x81aa8295, 0xa2d8, 0x412f, {0x89, 0xcd, 0xdb, 0x16, 0x48, 0x78, 0x50, 0x1f}};

// {C32F8F3C-2E4F-4edf-BA31-2630EE0457AE}
static const GUID FCATEGORY_SinkFilter = 
{0xc32f8f3c, 0x2e4f, 0x4edf, {0xba, 0x31, 0x26, 0x30, 0xee, 0x4, 0x57, 0xae}};

#endif //_FILTER_CATEGORY_