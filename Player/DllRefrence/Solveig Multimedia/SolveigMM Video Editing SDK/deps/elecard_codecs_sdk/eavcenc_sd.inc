/*/////////////////////////////////////////////////////////////////////////////
//
//    Elecard MPEG-4 part 10 (AVC/H.264) encoder. (ISO/IEC 14496-10:2005)
//
///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2007 Elecard Ltd.
//  All rights are reserved.  Reproduction in  whole or in  part is prohibited
//  without the written consent of the copyright owner.
//
//  Elecard Ltd.  reserves the  right to  make changes  without notice  at any
//  time.  Elecard Ltd. makes no  warranty,  expressed, implied  or statutory,
//  including but not  limited to any  implied  warranty of merchantability of
//  fitness for any particular purpose, or that the use  will not infringe any
//  third party patent, copyright or trademark.
//
//  Elecard Ltd. must  not be liable  for any loss or  damage arising from its
//  use.
//
/////////////////////////////////////////////////////////////////////////////*/

#ifndef AVCENC_AVCENC_GUIDS_SD_INC
#define AVCENC_AVCENC_GUIDS_SD_INC

//Parameters GUIDs are available in FULL version of SDK

#endif
