///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2007-2008 Elecard.
//  All rights reserved. Whole or part reproduction is prohibited
//  without the written consent of the copyright owner.
//
//  Elecard reserves the right to make changes at any time without the notice.
//  Elecard does not provide expressed, implied or statutory warranty,
//  including it but is not limited by other warranty of merchantability of
//  fitness for any particular purpose. The use will be not infringe any
//  third party patent, copyright or trademark.
//
//  Elecard must not be liable for any loss or damage resulting from the use.
//
///////////////////////////////////////////////////////////////////////////////
//
//  author:     Melnikov Grigory
//
//  purpose:	
//
///////////////////////////////////////////////////////////////////////////////

#if !defined _EM2VE_INC_SD_
#define _EM2VE_INC_SD_

//Parameters GUIDs are available in FULL version of SDK

#endif 