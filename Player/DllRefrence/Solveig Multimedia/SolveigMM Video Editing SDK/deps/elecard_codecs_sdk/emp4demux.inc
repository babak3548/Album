//////////////////////////////////////////////////////////////////////////
//
//	created:	2008/03/14
//	file name:	emp4demux.inc
//
//////////////////////////////////////////////////////////////////////////
//
//	Copyright (c) 2008 Elecard Ltd.
//	All rights are reserved.  Reproduction in whole or in part is prohibited
//	without the written consent of the copyright owner.
//
//	Elecard Ltd. reserves the right to make changes without
//	notice at any time. Elecard Ltd. makes no warranty, expressed,
//	implied or statutory, including but not limited to any implied
//	warranty of merchantability of fitness for any particular purpose,
//	or that the use will not infringe any third party patent, copyright
//	or trademark.
//
//	Elecard Ltd. must not be liable for any loss or damage arising
//	from its use.
//
//////////////////////////////////////////////////////////////////////////
//
//	author:  Denis Gordienko (Denis.Gordienko@elecard.ru)
//	
//	purpose: GUIDs for Elecard MP4 Demultiplexer
//
//////////////////////////////////////////////////////////////////////////

#if !defined(__EMP4_DEMUX_H__)
#define __EMP4_DEMUX_H__

//Parameters GUIDs are available in FULL version of SDK

#endif //#if !defined(__EMP4_DEMUX_H__)