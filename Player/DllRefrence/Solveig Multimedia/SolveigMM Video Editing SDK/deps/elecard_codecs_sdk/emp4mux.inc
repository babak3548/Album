//////////////////////////////////////////////////////////////////////////
//
//	created:	2008/22/04
//	file name:	mpeg4mux.inc
//
//////////////////////////////////////////////////////////////////////////
//
//	Copyright (c) 2007 Elecard Ltd.
//	All rights are reserved.  Reproduction in whole or in part is prohibited
//	without the written consent of the copyright owner.
//
//	Elecard Ltd. reserves the right to make changes without
//	notice at any time. Elecard Ltd. makes no warranty, expressed,
//	implied or statutory, including but not limited to any implied
//	warranty of merchantability of fitness for any particular purpose,
//	or that the use will not infringe any third party patent, copyright
//	or trademark.
//
//	Elecard Ltd. must not be liable for any loss or damage arising
//	from its use.
//
//////////////////////////////////////////////////////////////////////////
//
 //	author:  Victor Klykov, Victor.Klykov@elecard.ru
//	
//	purpose: GUIDs for MPEG-4 Muxer
//
//////////////////////////////////////////////////////////////////////////

#ifndef __MPEG4MUX_GUID_H__
#define __MPEG4MUX_GUID_H__

//Parameters GUIDs are available in FULL version of SDK

#endif //__MPEG4MUX_GUID_H__