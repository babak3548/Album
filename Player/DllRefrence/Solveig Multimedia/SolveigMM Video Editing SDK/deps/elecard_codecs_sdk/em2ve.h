//////////////////////////////////////////////////////////////////////////
//
//	created:	2005/05/12
//	file name:	propID_Mpeg2Enc.h
//
//////////////////////////////////////////////////////////////////////////
//
//	Copyright (c) 2005 Elecard Ltd.
//	All rights are reserved.  Reproduction in whole or in part is prohibited
//	without the written consent of the copyright owner.
//
//	Elecard Ltd. reserves the right to make changes without
//	notice at any time. Elecard Ltd. makes no warranty, expressed,
//	implied or statutory, including but not limited to any implied
//	warranty of merchantability of fitness for any particular purpose,
//	or that the use will not infringe any third party patent, copyright
//	or trademark.
//
//	Elecard Ltd. must not be liable for any loss or damage arising
//	from its use.
//
//////////////////////////////////////////////////////////////////////////
//
//	author:  Pavel Potapov
//	
//	purpose: The definition of the Elecard Mpeg2 Encoder filter's CLSID and CLSIDs of its parameters
//
//////////////////////////////////////////////////////////////////////////


#if !defined(__EM2VE_HEADER__)
#define __EM2VE_HEADER__
//#include "emcommon.h"

/*****************************************
	Filter GUIDs
*****************************************/
/*****************************************
	Parameters GUIDs
*****************************************/
//Parameters GUIDs are available in FULL version of SDK


#endif // __EM2VE_HEADER__