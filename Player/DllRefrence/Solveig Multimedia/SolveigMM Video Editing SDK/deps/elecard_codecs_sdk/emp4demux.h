//////////////////////////////////////////////////////////////////////////
//
//	created:	25/3/2008
//	file name:	MP4DemuxerParams.h
//
//////////////////////////////////////////////////////////////////////////
//
//	Copyright (c) 2008 Elecard Ltd.
//	All rights are reserved.  Reproduction in whole or in part is prohibited
//	without the written consent of the copyright owner.
//
//	Elecard Ltd. reserves the right to make changes without
//	notice at any time. Elecard Ltd. makes no warranty, expressed,
//	implied or statutory, including but not limited to any implied
//	warranty of merchantability of fitness for any particular purpose,
//	or that the use will not infringe any third party patent, copyright
//	or trademark.
//
//	Elecard Ltd. must not be liable for any loss or damage arising
//	from its use.
//
//////////////////////////////////////////////////////////////////////////
//
//	author:  Denis Gordienko (Denis.Gordienko@elecard.ru)
//	
//	purpose: MP4 Demultiplxer parameters GUIDs and values
//
//////////////////////////////////////////////////////////////////////////


//Parameters GUIDs are available in FULL version of SDK
