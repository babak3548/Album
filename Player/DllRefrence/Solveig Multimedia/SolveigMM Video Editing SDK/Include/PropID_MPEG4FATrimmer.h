
#ifndef __IMP4TRIMMERFAPROPS_H__
#define __IMP4TRIMMERFAPROPS_H__

#ifdef __cplusplus
extern "C" {
#endif


// {3484E68E-9CDD-4a77-A09E-0E640C11C2F6}
DEFINE_GUID(CLSID_AVC_TRIM_FA, 
         0x3484e68e, 0x9cdd, 0x4a77, 0xa0, 0x9e, 0xe, 0x64, 0xc, 0x11, 0xc2, 0xf6);

// {0E841274-E0DB-4ee1-B605-018A985CA4EE}
DEFINE_GUID(CLSID_AVC_TRIM_FA_PPage, 
         0xe841274, 0xe0db, 0x4ee1, 0xb6, 0x5, 0x1, 0x8a, 0x98, 0x5c, 0xa4, 0xee);

// {087B081D-7083-4706-B7C5-A427258FF862}
DEFINE_GUID(CLSID_AVC_TRIM_FA_About_PPage, 
         0x87b081d, 0x7083, 0x4706, 0xb7, 0xc5, 0xa4, 0x27, 0x25, 0x8f, 0xf8, 0x62);

#ifdef __cplusplus
}
#endif

#endif // __IMP4TRIMMERFAPROPS_H__

