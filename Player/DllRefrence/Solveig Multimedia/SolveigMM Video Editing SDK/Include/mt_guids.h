#pragma once

///////////////////////////////////////////////////////////////////////////////////
//video
///////////////////////////////////////////////////////////////////////////////////

//{1AC0BEBD-4D2B-45AD-BCEB-F2C41C5E3788}
static const GUID MEDIASUBTYPE_Matroska = 
{ 0x1AC0BEBD, 0x4D2B, 0x45AD, { 0xBC, 0xEB, 0xF2, 0xC4, 0x1C,0x5E,0x37,0x88}};

static const GUID MEDIASUBTYPE_H264_ANNEXB = 
{ 0x8D2D71CB, 0x243F, 0x45E3, { 0xB2, 0xD8, 0x5F, 0xD7, 0x96, 0x7E, 0xC0, 0x9B } };

static const GUID MEDIASUBTYPE_H264_2 = 
{ 0x31435641, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_H264_3 = 
{ 0x48535356, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_H264_4 = 
{ 0x34363248, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_MPEG4 = 
{ 0x7634706D, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };
static const GUID MEDIASUBTYPE_DIVX = 
{ 0x30355844, 0x0000 ,0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_DIVX_2 = 
{ 0x33564944, 0x0000 ,0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_DIVX_3 = 
{ 0x58564944, 0x0000 ,0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_XVID = 
{ 0x44495658, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_MP4V = 
{0x5634504d, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };


///////////////////////////////////////////////////////////////////////////////////
//audio
///////////////////////////////////////////////////////////////////////////////////

//8D2FD10B-5841-4a6b-8905-588FEC1ADED9
static const GUID MEDIASUBTYPE_Vorbis2 =
{0x8D2FD10B,0x5841,0x4a6b,{0x89,0x05,0x58,0x8F,0xEC,0x1A,0xDE,0xD9}};
static const GUID MEDIAFORMAT_Vorbis2 =
{0xB36E107F,0xA938,0x4387,{0x93,0xC7,0x55,0xE9,0x66,0x75,0x74,0x73}};
struct VORBISFORMAT2{
	DWORD Channels;
	DWORD SamplesPerSec;
	DWORD BitsPerSample;
	DWORD HeaderSize[3];
};
static const GUID MEDIASUBTYPE_FLAC =
{0x1541C5C0,0xCDDF,0x477d,{0xBC,0x0A,0x86,0xF8,0xAE,0x7F,0x83,0x54}};

static const GUID MEDIASUBTYPE_AC3 = 
{ 0x00002000, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_WAVE_DTS = 
{ 0x00002001, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };


#ifndef __wmcodecdsp_h__
//{eb27cec4-163e-4ca3-8b74-8e25f91b517e}
static const GUID MEDIASUBTYPE_DOLBY_TRUEHD = 
{ 0xeb27cec4, 0x163e, 0x4ca3, {  0x8b, 0x74, 0x8e, 0x25, 0xf9, 0x1b, 0x51, 0x7e } };

static const GUID MEDIASUBTYPE_DTS_HD = 
{ 0xa2e58eb7, 0xfa9, 0x48bb, {  0xa4, 0xc, 0xfa, 0xe, 0x15, 0x6d, 0x06, 0x45 } };


static const GUID MEDIASUBTYPE_DOLBY_DDPLUS = 
{ 0xa7fb87af, 0x2d02, 0x42fb, {  0xa4, 0xd4, 0x5, 0xcd, 0x93, 0x84, 0x3b, 0xdd } };

#endif

static const GUID MEDIASUBTYPE_AAC = 
{ 0x000000FF, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_MP3 = 
{ 0x00000055, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

static const GUID MEDIASUBTYPE_VOXWARE_AUDIO = 
{ 0x00000075, 0x0000, 0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

// {D2855FA9-61A7-4db0-B979-71F297C17A04}
static const GUID MEDIASUBTYPE_OGG =
{ 0xd2855fa9, 0x61a7, 0x4db0, 0xb9, 0x79, 0x71, 0xf2, 0x97, 0xc1, 0x7a, 0x4 };


// { 0000676F-0000-0010-8000-00AA00389B71} // Ogg Vorbis (mode 1)
static const GUID MEDIASUBTYPE_OGG_1 =
{ 0x0000676F, 0x0000 ,0x0010, { 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 } };

// cddca2d5-6d75-4f98-840e-737bedd5c63b
static const GUID MEDIASUBTYPE_VORBIS = 
{ 0xcddca2d5, 0x6d75, 0x4f98, 0x84, 0x0e, 0x73, 0x7b, 0xed, 0xd5, 0xc6, 0x3b };

//324d4451-0000-0010-8000-00AA00389B71
static const GUID MEDIASUBTYPE_QDM2 =
{ 0x324d4451, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71};

///////////////////////////////////////////////////////////////////////////////////
//subtitles 
///////////////////////////////////////////////////////////////////////////////////

//{E487EB08-6B26-4be9-9DD3-993434D313FD}
static const GUID MEDIATYPE_Subtitle = 
{ 0xE487EB08, 0x6B26, 0x4be9, { 0x9D, 0xD3, 0x99, 0x34, 0x34, 0xD3, 0x13, 0xFD } };

//{A33D2F7D-96BC-4337-B23B-A8B9FBC295E9}
static const GUID FORMAT_SubtitleInfo = 
{ 0xA33D2F7D, 0x96BC, 0x4337, { 0xB2, 0x3B, 0xA8, 0xB9, 0xFB, 0xC2, 0x95, 0xE9 } };

// {3020560F-255A-4ddc-806E-6C5CC6DCD70A}
static const GUID MEDIASUBTYPE_SSA = 
{ 0x3020560F, 0x255A, 0x4ddc, { 0x80, 0x6E, 0x6C, 0x5C, 0xC6, 0xDC, 0xD7, 0x0A } };

// {326444F7-686F-47ff-A4B2-C8C96307B4C2}
static const GUID MEDIASUBTYPE_ASS = 
{ 0x326444F7, 0x686F, 0x47ff, { 0xA4, 0xB2, 0xC8, 0xC9, 0x63, 0x07, 0xB4, 0xC2 } };

static const GUID MEDIASUBTYPE_UTF8 =
{0x87c0b230, 0x3a8, 0x4fdf, {0x80, 0x10, 0xb2, 0x7a, 0x58, 0x48, 0x20, 0xd}};

//F7239E31-9599-4e43-8DD5-FBAF75CF37F1
static const GUID  MEDIASUBTYPE_VOBSUB =
{0xF7239E31,0x9599,0x4e43,{0x8D,0xD5,0xFB,0xAF,0x75,0xCF,0x37,0xF1}};

//B753B29A-0A96-45be-985F-68351D9CAB90
static const GUID  MEDIASUBTYPE_USF =
{0xB753B29A,0x0A96,0x45be,{0x98,0x5F,0x68,0x35,0x1D,0x9C,0xAB,0x90}};

struct SUBTITLEINFO 
{
	DWORD dwOffset; // size of the structure/pointer to codec init data
	CHAR IsoLang[4]; // three letter lang code + terminating zero
	WCHAR TrackName[256]; // 256 bytes ought to be enough for everyone
};
//////////////////////////////////////////////////////////////////////////